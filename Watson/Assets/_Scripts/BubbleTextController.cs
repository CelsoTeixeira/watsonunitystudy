﻿using System.Collections.Generic;
using UnityEngine;

namespace i9.Watson
{
    public class BubbleTextController : MonoBehaviour
    {
        #region Singleton

        private static BubbleTextController _instance;

        public static BubbleTextController Instance
        {
            get
            {
                if (_instance == null)
                    _instance = GameObject.FindObjectOfType<BubbleTextController>();
                return _instance;
            }
        }

        #endregion

        public BubbleTextVisualizer BubblePrefab;
        public Transform BubbleHolder;

        public List<BubbleTextVisualizer> BubbleList = new List<BubbleTextVisualizer>();

        public void AddNewBubble(string bubbleText)
        {
            BubbleTextVisualizer newInstance = Instantiate(BubblePrefab, BubbleHolder);
            newInstance.transform.localScale = Vector3.one;
            newInstance.UpdateText(bubbleText);

            BubbleList.Add(newInstance);
        }
    }
}