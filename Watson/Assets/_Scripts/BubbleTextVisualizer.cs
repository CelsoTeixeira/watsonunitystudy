﻿using TMPro;
using UnityEngine;

namespace i9.Watson
{
    public class BubbleTextVisualizer : MonoBehaviour
    {
        public TextMeshProUGUI BubbleText;

        public void UpdateText(string toUpdate)
        {
            BubbleText.text = toUpdate;
        }
    }
}