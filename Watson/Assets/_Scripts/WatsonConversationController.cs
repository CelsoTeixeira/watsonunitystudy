﻿using System.Collections;
using System.Text;
using IBM.Watson.DeveloperCloud.Services.Conversation.v1;
using TMPro;
using UnityEngine;

namespace i9.Watson
{
//    private Conversation m_Conversation = new Conversation();
//    private string m_WorkspaceID = "car_demo_1";
//    private string m_Input = "Can you unlock the door?";

//    void Start()
//    {
//      Debug.Log("User: " + m_Input);
//      m_Conversation.Message(OnMessage, m_WorkspaceID, m_Input);
//    }

//    void OnMessage(MessageResponse resp, string customData)
//    {
//      foreach (Intent mi in resp.intents)
//      Debug.Log("intent: " + mi.intent + ", confidence: " + mi.confidence);

//      Debug.Log("response: " + resp.output.text);
//    }

    public class WatsonConversationController : MonoBehaviour
    {
        public TMP_InputField InputField;

        private Conversation _conversation;

        private string _workspaceId = "d770cb43-8017-4f27-b70a-8d4a0b853c2f";

        private void Awake()
        {
            _conversation = new Conversation();            
        }

        //NOTE(Celso): This should be called from the UI Button.
        public void SendInputText()
        {
            if (string.IsNullOrEmpty(InputField.text))
            {
                UnityEngine.Debug.Log("The input field is empty... Don't send anythig to Watson.");
                return;
            }

            UnityEngine.Debug.Log("Sending input to user!");
            string toSend = InputField.text;
            InputField.text = "";
            
            _conversation.Message(OnMessage, _workspaceId, toSend);

            BubbleTextController.Instance.AddNewBubble(toSend);
        }

        private void OnMessage(MessageResponse watResponse, string customData)
        {
            UnityEngine.Debug.Log("Watson message callback!");
            //UnityEngine.Debug.Log("Watson response: " + watResponse.output.text[0]);

            StringBuilder builder = new StringBuilder();

            for (int i = 0; i < watResponse.output.text.Length; i++)
            {
                builder.Append(watResponse.output.text[i]).AppendLine();
            }

            BubbleTextController.Instance.AddNewBubble(builder.ToString());
        }
    }

    
}